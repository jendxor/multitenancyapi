/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.service;

import com.example.dto.ServiceResult;
import com.example.model.Employee;
import com.example.repository.EmployeeRepo;
import com.example.util.TenantContextHolder;
import com.example.util.Utils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

/**
 *
 * @author JIDEX
 */
@Service
public class AppService {

    @Autowired
    EmployeeRepo employeeRepo;
    @Autowired
    private JavaMailSender sender;
    @Autowired
    private Configuration freemarkerConfig;
   
    public EmployeeRepo getEmployeeRepo() {
        return employeeRepo;
    }

    private JasperReport loadTemplate() throws JRException, IOException {
        ClassPathResource file = new ClassPathResource("/einvoice.jrxml");
        final JasperDesign jasperDesign = JRXmlLoader.load(file.getInputStream());

        return JasperCompileManager.compileReport(jasperDesign);
    }

    private Map<String, Object> parameters() throws IOException {
        ClassPathResource file = new ClassPathResource("/images/cabsol.png");
        final Map<String, Object> parameters = new HashMap<>();
        TenantContextHolder.setTenantId(Utils.TENANT+"1");
        List<Employee> list = employeeRepo.findAll();
        parameters.put("maindatasource", list);
        parameters.put("logo", file.getFile().getAbsolutePath());

        return parameters;
    }

    public ByteArrayInputStream getReports() throws IOException {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        File file =File.createTempFile("invoice", ".pdf");
        try {
            final JasperReport report = loadTemplate();

            // Create parameters map.
            final Map<String, Object> parameters = parameters();
            JasperPrint print = JasperFillManager.fillReport(report, parameters);

            JasperExportManager.exportReportToPdfStream(print, out);

        } catch (final JRException | IOException e) {
            System.out.println(e.getMessage());
        }
      return new ByteArrayInputStream(out.toByteArray());
    }

    public ServiceResult sendMessage(String content,
            String subjects, String to, String user) throws Exception {
        ServiceResult results = new ServiceResult();
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        Map<String, Object> model = new HashMap();
        model.put("message", content);
        model.put("user", user);

        //model.put("logo", image);
        // set loading location to src/main/resources
        // You may want to use a subfolder such as /templates here
        try {
            freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");
            //freemarkerConfig.setDirectoryForTemplateLoading(new ClassPathResource("/templates").getFile());
            Template t = freemarkerConfig.getTemplate("message.ftl");
            String text = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
            helper.setTo(to);
            helper.setFrom(Utils.ADMIN_EMAIL);
            helper.setText(text, true); // set to html
            helper.setSubject(subjects);
            //InputStreamSource reports = getReports();
            helper.addAttachment("invoice", new ByteArrayResource(IOUtils.toByteArray(getReports())));
            sender.send(message);
            results.setRespmessage(Utils.MESSAGESENT);
            results.setRespcode("OK");
            results.setResponsestatus("200");
        } catch (IOException | TemplateException | MessagingException | MailException e) {
            results.setRespmessage("Error: " + e.getMessage());
        }
        return results;
    }

}
