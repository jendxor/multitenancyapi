
package com.example.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author JIDEX
 * @version 1.0
 * @since 1.0 (OCTOMBER 2018)
 *
 */
public interface CustomUserDetailsService {
    
    UserDetails loadUserByUsernameAndTenantname(String username, String tenantName) throws UsernameNotFoundException;
}
