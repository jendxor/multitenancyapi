/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.controller;

import com.example.dto.Email;
import com.example.dto.Employeedto;
import com.example.dto.Response;
import com.example.dto.ServiceResult;
import com.example.mapper.DtoMapper;
import com.example.model.Users;
import com.example.multitenancy.MultiTenancyJpaConfiguration;
import com.example.service.AppService;
import com.example.service.UserService;
import com.example.util.ConnectionParams;
import com.example.util.ConnectionUtils;
import com.example.util.TenantContextHolder;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author JIDEX
 */
@RestController
@RequestMapping(value = "/accounts", headers = "Accept=application/json")
public class AppController {
    
    @Autowired
    UserService service;
    @Autowired
    AppService appService;
    ////////Inject application datasource
    @Autowired
    Map<String, DataSource> dataSourcesMtApp;
    
    @PostMapping("/invoice-report")
    ServiceResult getReport(@RequestBody Email email) throws Exception{
        return appService.sendMessage(email.getContent(), email.getSubjects(), email.getTo(), email.getUser());
    }
    public void updateDataSource(String url, String username, String password, String tenant) {
        try {
            DataSourceBuilder factory1 = DataSourceBuilder.create(MultiTenancyJpaConfiguration.class.getClassLoader()).url(url)
                    .username(username).password(password)
                    .driverClassName("com.mysql.jdbc.Driver");
            dataSourcesMtApp.put(tenant, factory1.build());
            System.out.println("Size:......................................................" + dataSourcesMtApp.size());
        } catch (Exception ex) {
            Logger.getLogger(AppController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @PostMapping("/create-account")
    public Response createAccount(@RequestBody ConnectionParams request) {
        String tenant = ConnectionUtils.initializeDatabase(request.getDatabase(), request.getDbusername(), request.getDbpassword());
        updateDataSource("jdbc:mysql://localhost:3306/" + request.getDatabase() + "?useSSL=false", request.getDbusername(), request.getDbpassword(), tenant);
        TenantContextHolder.setTenantId(tenant);
        Users user = new Users();
        user.setPassword(request.getLoginpassword());
        user.setUsername(request.getLoginusername());
        user.setTenant(tenant);
        user = service.save(user);
        String response = "Account Setup Completed TenantId: " + tenant + " Username: " + user.getUsername();
        Response rp = new Response();
        rp.setResult(response);
        return rp;
    }
    
    @GetMapping("/employee-view")
    public Employeedto getEmployee(@RequestParam("id") Integer id,
            @RequestParam(value = "tenant", required = false) String tenant) {
        TenantContextHolder.setTenantId(tenant);
        return DtoMapper.maptoEmployeedto(service.getOne(id));
    }
}
