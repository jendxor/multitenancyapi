/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author JIDEX
 */
@Entity
@Table(name = "connectionsource")
@NamedQueries({
    @NamedQuery(name = "Connectionsource.findAll", query = "SELECT c FROM Connectionsource c"),
    @NamedQuery(name = "Connectionsource.findById", query = "SELECT c FROM Connectionsource c WHERE c.id = :id"),
    @NamedQuery(name = "Connectionsource.findByUsername", query = "SELECT c FROM Connectionsource c WHERE c.username = :username"),
    @NamedQuery(name = "Connectionsource.findByPassword", query = "SELECT c FROM Connectionsource c WHERE c.password = :password"),
    @NamedQuery(name = "Connectionsource.findByPort", query = "SELECT c FROM Connectionsource c WHERE c.port = :port"),
    @NamedQuery(name = "Connectionsource.findByUrls", query = "SELECT c FROM Connectionsource c WHERE c.urls = :urls"),
    @NamedQuery(name = "Connectionsource.findByDrivers", query = "SELECT c FROM Connectionsource c WHERE c.drivers = :drivers"),
    @NamedQuery(name = "Connectionsource.findByDatecreated", query = "SELECT c FROM Connectionsource c WHERE c.datecreated = :datecreated")})
public class Connectionsource implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "username")
    private String username;
    @Size(max = 100)
    @Column(name = "password")
    private String password;
    @Column(name = "port")
    private Integer port;
    @Size(max = 100)
    @Column(name = "urls")
    private String urls;
    @Size(max = 100)
    @Column(name = "drivers")
    private String drivers;
    @Column(name = "datecreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreated;

    public Connectionsource() {
    }

    public Connectionsource(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUrls() {
        return urls;
    }

    public void setUrls(String urls) {
        this.urls = urls;
    }

    public String getDrivers() {
        return drivers;
    }

    public void setDrivers(String drivers) {
        this.drivers = drivers;
    }

    public Date getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Date datecreated) {
        this.datecreated = datecreated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Connectionsource)) {
            return false;
        }
        Connectionsource other = (Connectionsource) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.model.Connectionsource[ id=" + id + " ]";
    }
    
}
