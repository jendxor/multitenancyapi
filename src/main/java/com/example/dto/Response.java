/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.io.Serializable;

/**
 *
 * @author JIDEX
 */
@JsonRootName("response")
public class Response implements Serializable{
  private String result;
public Response(){
    
}
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
  
}
