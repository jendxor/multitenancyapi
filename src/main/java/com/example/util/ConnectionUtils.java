/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.util;

import com.example.dto.Connectiondto;
import com.example.dto.Connectionsourcedto;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.core.io.ClassPathResource;

/**
 *
 * @author JIDEX
 */
public class ConnectionUtils {
    
    public static String initializeDatabase(String newdatabasename,
            String newusername, String newpassword) {
        String response = null;
        try {
            Statement st = getConnect(getMasterData().getPort(), getMasterData().getName(), getMasterData().getUsername(), getMasterData().getPassword()).createStatement();
            System.out.println(createDatabaseAndCredentials(st, newdatabasename, newusername, newpassword));
            System.out.println(copyTable(getTables(st), st, newdatabasename, getMasterData().getName()));
            response = updateProperties(getMasterData().getPort(), getMasterData().getName(), getMasterData().getUsername(), getMasterData().getPassword(),
                    newusername, newpassword, newdatabasename, getMasterData().getPort());
            
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return response;
    }
    
    public static List<Connectionsourcedto> getDataSource() {
        List<Connectionsourcedto> list = new ArrayList<>();
        Connection con = getConnect(Utils.DBPORT, Utils.DATABASE, Utils.DBUSERNAME, Utils.DBPASSWORD);
        Statement st;
        try {
            
            st = con.createStatement();
            ResultSet rs = st.executeQuery("select * from connectionsource");
            while (rs.next()) {
                Connectionsourcedto data = new Connectionsourcedto();
                data.setDatecreated(rs.getDate("datecreated"));
                data.setDrivers(rs.getString("drivers"));
                data.setId(rs.getInt("id"));
                data.setPassword(rs.getString("password"));
                data.setPort(rs.getInt("port"));
                data.setUrls(rs.getString("urls"));
                data.setUsername(rs.getString("username"));
                list.add(data);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return list;
    }
    
    public static List<String> getTables(Statement st) {
        List<String> tables = new ArrayList<>();
        try {
            
            DatabaseMetaData dbmd = st.getConnection().getMetaData();
            String[] types = {"TABLE"};
            ResultSet rs = dbmd.getTables(null, null, "%", types);
            System.out.println("Fetching Table");
            while (rs.next()) {
                tables.add(rs.getString("TABLE_NAME"));
            }
        } catch (SQLException e) {
        }
        return tables;
    }
    
    public static String copyTable(List<String> tables, Statement st, String newdb, String masterdb) {
        String result = "";
        for (String rs : tables) {
            String query = "create table " + newdb + "." + rs + " like " + masterdb + "." + rs;
            try {
                st.execute(query);
                result = "Table Created";
            } catch (SQLException ex) {
                // result = ex.getMessage();
                Logger.getLogger(ConnectionUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    public static Connection getConnect(int port, String database, String username, String password) {
        Connection con = null;
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:" + port + "/" + database, username, password);
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ConnectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }
    
    public static String createDatabaseAndCredentials(Statement st, String database, String username, String password) {
        String response = null;
        try {
            st.execute("create database " + database);
            int result = st.executeUpdate("grant all privileges on " + database + ".* to '" + username + "'@'localhost' identified by '" + password + "'");
            response = result == 0 ? "Database Created, User Created and Permission Granted" : "Unable to create database credentials please contact Administrator";
        } catch (SQLException ex) {
            //response = ex.getMessage();
            Logger.getLogger(ConnectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
        
    }
    
    public static String updateProperties(int masterport, String masterdatabasename,
            String masterusername, String masterpassword, String username,
            String password, String database, int port) {
        int generatedKey = 0;
        String query = "INSERT INTO connectionsource(username, password, port, urls, drivers, datecreated) values(?,?,?,?,?,?)";
        Connection con = getConnect(port, masterdatabasename, masterusername, masterpassword);
        PreparedStatement st;
        Calendar cl = Calendar.getInstance();
        try {
            st = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, username);
            st.setString(2, password);
            st.setInt(3, port);
            st.setString(4, "jdbc:mysql://localhost:" + port + "/" + database);
            st.setString(5, "com.mysql.jdbc.Driver");
            st.setDate(6, new java.sql.Date(cl.getTimeInMillis()));
            st.execute();
            ResultSet rs = st.getGeneratedKeys();
            
            if (rs.next()) {
                generatedKey = rs.getInt(1);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Utils.TENANT + generatedKey;
    }
    
    public static Connectiondto getMasterData() {
        Properties prop = new Properties();
        InputStream input = null;
        Connectiondto con = new Connectiondto();
        try {
            ClassPathResource file = new ClassPathResource("/config.properties");
            input = new FileInputStream(file.getFile());

            // load a properties file
            prop.load(input);
            con.setName(prop.getProperty("database.name"));
            con.setPort(Integer.valueOf(prop.getProperty("database.port")));
            con.setPassword(prop.getProperty("database.password"));
            con.setUsername(prop.getProperty("database.username"));
            
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return con;        
    }

//    public static String updateProperties(File file, String username, String password, String database, int port, String tenantid) {
//        String response = null;
//        BufferedWriter writer;
//        try {
//            writer = new BufferedWriter(new FileWriter(file, true));
//            StringBuilder b = new StringBuilder();
//            b.append("\n      -");
//            b.append("\n        tenantId: ").append(tenantid);
//            b.append("\n        url: jdbc:mysql://localhost:").append(port).append("/").append(database).append("?useSSL=false ");
//            b.append("\n        username: ").append(username).append("");
//            b.append("\n        password: ").append(password).append("");
//            b.append("\n        driverClassName: com.mysql.jdbc.Driver");
//            //writer.append(b);
//            writer.close();
//            response = "Success: Account Setup Completed";
//        } catch (IOException ex) {
//            //response = ex.getMessage();
//            Logger.getLogger(ConnectionUtils.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return response;
//    }
}
