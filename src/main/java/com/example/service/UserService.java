/**
 *
 */
package com.example.service;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.model.Employee;
import com.example.model.Users;
import com.example.repository.EmployeeRepo;
import com.example.repository.UserRepository;

/**
 * Implementation of the {@link UserService} which accesses the {@link User}
 * entity. This is the recommended way to access the entities through an
 * interface rather than using the corresponding repository. This allows for
 * separation into repository code and the service layer.
 *
 * @author JIDEX
 * @version 1.0
 * @since 1.0 (OCTOMBER 2018)
 */
@Service
public class UserService{

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmployeeRepo employeeRepo;
    @Autowired
    private PasswordEncoder passwordEncoder;

    /*
     * (non-Javadoc)
     * 
     * @see com.example.service.UserService#save(com.example.model.User)
     */
   
    public Users save(Users user) {
        // TODO Auto-generated method stub
        user.setDatecreated(new Date());
        user.setActive(true);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.service.UserService#findLoggedInUsername()
     */
    
    public String findLoggedInUsername() {
        Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (userDetails instanceof UserDetails) {
            String username = ((UserDetails) userDetails).getUsername();
            LOG.info("Logged in username:" + username);
            return username;
        }

        return null;
    }

    
    public Users findByUsernameAndTenantname(String username, String tenant) {
        Users user = userRepository.findByUsernameAndTenantname(username, tenant);
        LOG.info("Found user with username:" + user.getUsername() + " from tenant:" + user.getTenant());
        return user;
    }

    public Employee getOne(Integer id) {
        return employeeRepo.findOne(id);
    }

}
